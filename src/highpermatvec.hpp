//
// This file is part of the course materials for AMATH483/583 at the University of Washington,
// Spring 2018
//
// Licensed under Creative Commons Attribution-NonCommercial-ShareAlike 4.0
// International License
// https://creativecommons.org/licenses/by-nc-sa/4.0/
//
// Author: Andrew Lumsdaine
//

#ifndef __HIGHPERMATVEC_HPP
#define __HIGHPERMATVEC_HPP

#include "Vector.hpp"
#include "Matrix.hpp"

Vector operator*(const Matrix& A, const Vector& x);
void matvec(const Matrix& A, const Vector& x, Vector& y);

#endif // __HIGHPERMATVEC_HPP
