//
// This file is part of the course materials for AMATH483/583 at the University of Washington,
// Spring 2018
//
// Licensed under Creative Commons Attribution-NonCommercial-ShareAlike 4.0
// International License
// https://creativecommons.org/licenses/by-nc-sa/4.0/
//
// Author: Andrew Lumsdaine
//

#ifndef __HIGHPERMATMAT_HPP
#define __HIGHPERMATMAT_HPP

#include "Matrix.hpp"

void multiply(const Matrix& A, const Matrix& B, Matrix& C);
void basicMultiply(const Matrix& A, const Matrix& B, Matrix& C);
void hoistedMultiply(const Matrix& A, const Matrix& B, Matrix& C);
void tiledMultiply2x2(const Matrix& A, const Matrix& B, Matrix& C);
void tiledMultiply2x4(const Matrix& A, const Matrix& B, Matrix& C);
void tiledMultiply4x2(const Matrix& A, const Matrix& B, Matrix& C);
void tiledMultiply4x4(const Matrix& A, const Matrix& B, Matrix& C);
void hoistedTiledMultiply2x2(const Matrix& A, const Matrix& B, Matrix& C);
void blockedTiledMultiply2x2(const Matrix& A, const Matrix& B, Matrix& C);
void hoistedBlockedTiledMultiply2x2(const Matrix& A, const Matrix& B, Matrix& C);
void copyBlockedTiledMultiply2x2(const Matrix& A, const Matrix& B, Matrix& C);
void hoistedCopyBlockedTiledMultiply2x2(const Matrix& A, const Matrix& B, Matrix& C);
void hoistedCopyBlockedTiledMultiply4x4(const Matrix& A, const Matrix& B, Matrix& C);
Matrix operator*(const Matrix& A, const Matrix& B);

#endif    // __HIGHPERMATMAT_HPP
