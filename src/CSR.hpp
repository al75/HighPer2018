//
// This file is part of the course materials for AMATH483/583 at the University of Washington,
// Spring 2018
//
// Licensed under Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License
// https://creativecommons.org/licenses/by-nc-sa/4.0/
//
// Author: Andrew Lumsdaine
//

#ifndef CSR_HPP
#define CSR_HPP

#include <cassert>
#include <iostream>
#include <fstream>
#include <utility>
#include <vector>
#include <algorithm>
#include "Vector.hpp"

class CSRMatrix {
public:
  CSRMatrix(size_t M, size_t N) : is_open(false), num_rows_(M), num_cols_(N), row_indices_(num_rows_+1, 0)  {}

  void open_for_push_back()  { is_open = true; }

  void close_for_push_back() { 
    for (size_t i = 0; i < num_rows_; ++i) {
      row_indices_[i+1] += row_indices_[i];
    }
    for (size_t i = num_rows_; i > 0; --i) {
      row_indices_[i] = row_indices_[i-1];
    }
    row_indices_[0] = 0;

    is_open = false; 
  }

  void push_back(size_t i, size_t j, double value) {
    assert(is_open);
    assert(i < num_rows_ && i >= 0);
    assert(j < num_cols_ && j >= 0);

    ++row_indices_[i];
    col_indices_.push_back(j);
    storage_.push_back(value);
  }

  void matvec(const Vector& x, Vector& y) const;


  void clear() {
    col_indices_.clear();
    storage_.clear();
    std::fill(row_indices_.begin(), row_indices_.end(), 0);
  }

  size_t num_rows()     const { return num_rows_; }
  size_t num_cols()     const { return num_cols_; }
  size_t num_nonzeros() const { return storage_.size(); }

private:
  bool is_open;
  size_t num_rows_, num_cols_;
  std::vector<size_t> row_indices_, col_indices_;
  std::vector<double> storage_;
};

#endif // CSR_HPP
