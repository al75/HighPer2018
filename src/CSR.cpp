//
// This file is part of the course materials for AMATH483/583 at the University of Washington,
// Spring 2018
//
// Licensed under Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License
// https://creativecommons.org/licenses/by-nc-sa/4.0/
//
// Author: Andrew Lumsdaine
//

#include "CSR.hpp"
#include "Matrix.hpp"
#include "Vector.hpp"
#include <cassert>
#include <fstream>
#include <iostream>
#include <string>

void CSRMatrix::matvec(const Vector& x, Vector& y) const {
  for (size_t i = 0; i < num_rows_; ++i) {
    for (size_t j = row_indices_[i]; j < row_indices_[i + 1]; ++j) {
      y(i) += storage_[j] * x(col_indices_[j]);
    }
  }
}
